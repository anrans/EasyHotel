﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.Admin.Lib;
using IEMSOFT.Foundation;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class PayTypeController : BaseController
    {
        [AllowAnonymous]
        public ActionResult Option(string defaultText, string defaultValue)
        {
            List<SelectListItem> options;
            using (var db = new EasyHotelDB())
            {
                var dal = new PayTypeDAL(db);
                var ret = dal.GetList();
                options = ret.Select((item) => new SelectListItem
                {
                    Text = item.Name,
                    Value = item.PayTypeId.ToString()
                }).ToList();
            }

            if (!string.IsNullOrEmpty(defaultText))
            {
                options.Insert(0, new SelectListItem { Selected=true,Text=defaultText,Value=defaultValue??""});
            }
            return JsonNet(options);
        }

    }
}
