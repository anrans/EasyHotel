﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.Admin.Lib;
using IEMSOFT.Foundation;
using IEMSOFT.EasyHotel.Admin.Models;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class BillController : BaseController
    {
        [HttpPost]
        public ActionResult Create(BillCreateModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            model.CreatedByUserId = CurrentUser.UserId;
            model.UpdatedByUserId = CurrentUser.UserId;
            BillBLL.Create(model);
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult PreSettle(int roomId)
        {
            var ret = new BasicResult<BillModel, List<string>>();
            ret.Msg = new List<string>();
           var  result=BillBLL.PreSettle(roomId, DateTime.Now, CurrentUser.UserId);
           ret.Data = result;
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult GetOnePendingByRoomId(int roomId)
        {
            var ret = new BasicResult<BillModel, List<string>>();
            ret.Msg = new List<string>();
            var result = BillBLL.GetOnePendingBill(DateTime.Now, roomId);
            ret.Data = result;
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult Settle(long billId,
                                   DateTime checkoutDate,
                                  int stayTotalDays,
                                  decimal roomTotalFee,
                                  decimal consumeFee,
                                  string consumeItem,
                                  string comments,
                                  int stayTotalHours)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            BillBLL.Settle(billId, checkoutDate, stayTotalDays, roomTotalFee,consumeFee,consumeItem,comments,stayTotalHours);
            return JsonNet(ret);
        }
    }
}
