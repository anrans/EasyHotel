﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.EasyHotel.DAL.Models;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class GroupHotelController : BaseController
    {
        //
        // GET: /GroupHotel/
        [AllowAnonymous]
        public ActionResult Option()
        {
            List<SelectListItem> options;
            using (var db = new EasyHotelDB())
            {
                var dal = new GroupHotelDAL(db);
                var ret = dal.GetList();
                options = ret.Select((item) => new SelectListItem
                {
                    Text = item.Name,
                    Value = item.GroupHotelId.ToString()
                }).ToList();
            }
            options.Insert(0, new SelectListItem { Text = CommonConsts.PleaseChoose, Value = CommonConsts.Zero });
            if (options.Count >= 2)
            {
                options[1].Selected = true;
            }
            else
            {
                options[0].Selected = true;
            }
            return JsonNet(options);
        }

        public ActionResult Index()
        {
            GroupHotelModel groupHotelModel = null;
            using (var db = new EasyHotelDB())
            {
                var dal = new GroupHotelDAL(db);
                var groupHotel = dal.GetOne(CurrentUser.GroupHotelId);
                groupHotelModel = AutoMapper.Mapper.Map<GroupHotelModel>(groupHotel);
            }
            return View(groupHotelModel);
        }

        [HttpPost]
        public ActionResult Save(GroupHotelModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            model.GroupHotelId = CurrentUser.GroupHotelId;
            using (var db = new EasyHotelDB())
            {
                var dal = new GroupHotelDAL(db);
                var groupHotel = AutoMapper.Mapper.Map<GroupHotel>(model);
                dal.Save(groupHotel);
            }
            return JsonNet(ret);
        }

    }
}
