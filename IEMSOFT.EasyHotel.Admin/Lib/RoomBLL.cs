﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.DAL.Models.Extension;
using IEMSOFT.Foundation;
namespace IEMSOFT.EasyHotel.Admin.Lib
{
    public class RoomBLL 
    {
        public static List<RoomModel> GetRooms(int subHotelId)
        {
            using (var db = new EasyHotelDB())
            {
                var dal = new RoomDAL(db);
                var ret = dal.GetList(subHotelId);
                var result = AutoMapper.Mapper.Map<List<RoomModel>>(ret);
                return result;

            }
        }

        public static void SetMending(int roomId, bool isMending)
        {
            using (var db = new EasyHotelDB())
            {
                var dal = new RoomDAL(db);
                dal.SetMending(roomId, isMending);
            }
        }

        public static List<SelectListItem> GetRoomStatusOption(DateTime checkinDate, int subHotelId, RoomStatusType roomStatusId)
        {
            var roomStatus = GetRoomStatus(checkinDate, subHotelId);
            roomStatus = roomStatus.FindAll(item => item.RoomStatusId == roomStatusId);
            List<SelectListItem> options;
            options = roomStatus.Select((item) => new SelectListItem
            {
                Text = string.Format("{0} {1}", item.RoomNo, item.RoomTypeName),
                Value = item.RoomId.ToString()
            }).ToList();
            return options;
        }

        public static List<RoomStatusModel> GetRoomStatus(DateTime checkinDate, int subHotelId)
        {
            var ret = new List<RoomStatusModel>();
            var rooms = GetRooms(subHotelId);
            var bills = new List<BillExtension>();
            using (var db = new EasyHotelDB())
            {
                var dal = new BillDAL(db);
                bills = dal.GetPendingBills(checkinDate, subHotelId);
            }
            foreach (var room in rooms)
            {
                var tmpRoomStatus = AutoMapper.Mapper.Map<RoomStatusModel>(room);
                if (tmpRoomStatus.IsMending)
                {
                    tmpRoomStatus.RoomStatusId = RoomStatusType.IsMending;
                    tmpRoomStatus.BillId = 0;
                }
                else
                {
                    var bill = bills.FirstOrDefault(item => item.RoomId == room.RoomId);
                    if (bill == null)
                    {
                        tmpRoomStatus.RoomStatusId = RoomStatusType.Normal;
                        tmpRoomStatus.BillId = 0;
                    }
                    else
                    {
                        tmpRoomStatus.RoomStatusId = RoomStatusType.CheckedIn;
                        tmpRoomStatus.BillId = bill.BillId;
                        tmpRoomStatus.CustomerCheckinDate = bill.CheckinDate.ToLongDateStr();
                        tmpRoomStatus.CustomerDeposit = bill.Deposit.ToString();
                        tmpRoomStatus.CustomerPayType = bill.PayTypeName;
                        tmpRoomStatus.CustomerTravelAgency = bill.TravelAgencyName;
                        tmpRoomStatus.SinglePrice = bill.SinglePrice.ToString();
                        tmpRoomStatus.BillCreateUserFullName = bill.CreateUserFullName;
                        tmpRoomStatus.CustomerName = bill.CustomerName;
                        tmpRoomStatus.IsHourRoom = bill.IsHourRoom.ToBool();
                    }
                }
                ret.Add(tmpRoomStatus);

            }
            return ret;
        }

        public static PrintCheckinModel GetPrintCheckinModel(BillCreateModel model)
        {
            var ret = new PrintCheckinModel();
            ret.BillNo = model.BillNo;
            ret.CheckinDate = model.CheckinDate;
            ret.CustomerIDNo = model.CustomerIDNo;
            ret.CustomerName = model.CustomerName;
            ret.Deposit = model.Deposit;
            ret.PrintTime = DateTime.Now.ToLongDateStr();
            ret.SinglePrice = model.SinglePrice;
            using (var db = new EasyHotelDB())
            {
                var roomDAL = new RoomDAL(db);
                var room = roomDAL.GetOne(model.RoomId);
                ret.RoomName = string.Format("{0} {1}",room.RoomNo,room.RoomTypeName);
                ret.RoomNo = room.RoomNo;
                ret.RoomTypeName = room.RoomTypeName;
                ret.SubhotelAddress = room.SubHotelAddress;
                ret.SubHotelName = room.SubHotelName;
                ret.SubhotelFax = room.SubHotelFax;
                ret.SubhotelPhone = room.SubHotelPhone;
                var travelAgencyDAL = new TravelAgencyDAL(db);
                var travelAgency = travelAgencyDAL.GetOne(model.TravelAgencyId);
                ret.TravelAgencyName = travelAgency == null ? "散客" : travelAgency.Name;
                var payTypeDAL = new PayTypeDAL(db);
                var payType = payTypeDAL.GetOne(model.PayTypeId);
                ret.PayTypeName = payType.Name;
                ret.IsHourRoom = model.IsHourRoom.ToBool();
                ret.MinFeeForHourRoom = model.MinFeeForHourRoom;
                ret.PricePerHour = model.PricePerHour;
                ret.Comments = model.Comments;
                if (model.BillId > 0)
                {
                    var billDAL = new BillDAL(db);
                   var bill= billDAL.GetOneBill(model.BillId);
                   model.CreatedByUserId = bill.CreatedByUserId;
                }
                var userDAL = new UserDAL(db);
               var user= userDAL.GetOneById(model.CreatedByUserId);
                ret.BillCreatedUser=user==null?"":user.FullName;
            }
            return ret;
        }


        public static PrintCheckoutModel GetPrintCheckoutModel(BillModel model)
        {
            var ret = new PrintCheckoutModel();
            ret.BillNo = model.BillNo;
            ret.CheckinDate = model.CheckinDate;
            ret.CustomerIDNo = model.CustomerIDNo;
            ret.CustomerName = model.CustomerName;
            ret.Deposit = model.Deposit;
            ret.PrintTime = DateTime.Now.ToLongDateStr();
            ret.SinglePrice = model.SinglePrice;
            ret.CheckoutDate = model.CheckoutDate;
            ret.StayTotalDays = model.StayTotalDays;
            ret.StayTotalHours = model.StayTotalHours;
            ret.RoomTotalFee = model.RoomTotalFee;
            ret.ConsumeFee = model.ConsumeFee;
            ret.ConsumeItem = model.ConsumeItem;
            ret.Sum = ret.ConsumeFee + ret.RoomTotalFee;
            using (var db = new EasyHotelDB())
            {
                var roomDAL = new RoomDAL(db);
                var room = roomDAL.GetOne(model.RoomId);
                ret.RoomName = string.Format("{0} {1}", room.RoomNo, room.RoomTypeName);
                ret.RoomNo = room.RoomNo;
                ret.RoomTypeName = room.RoomTypeName;
                ret.SubhotelAddress = room.SubHotelAddress;
                ret.SubHotelName = room.SubHotelName;
                ret.SubhotelFax = room.SubHotelFax;
                ret.SubhotelPhone = room.SubHotelPhone;
                var travelAgencyDAL = new TravelAgencyDAL(db);
                var travelAgency = travelAgencyDAL.GetOne(model.TravelAgencyId);
                ret.TravelAgencyName = travelAgency == null ? "散客" : travelAgency.Name;
                var payTypeDAL = new PayTypeDAL(db);
                var payType = payTypeDAL.GetOne(model.PayTypeId);
                ret.PayTypeName = payType.Name;
                ret.IsHourRoom = model.IsHourRoom.ToBool();
                ret.MinFeeForHourRoom = model.MinFeeForHourRoom;
                ret.PricePerHour = model.PricePerHour;
                ret.Comments = model.Comments;
                if (model.BillId > 0)
                {
                    var billDAL = new BillDAL(db);
                    var bill = billDAL.GetOneBill(model.BillId);
                    model.CreatedByUserId = bill.CreatedByUserId;
                }
                var userDAL = new UserDAL(db);
                var user = userDAL.GetOneById(model.CreatedByUserId);
                ret.BillCreatedUser = user == null ? "" : user.FullName;
            }
            return ret;
        }
    }
}