﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.Foundation.MVC;
using IEMSOFT.Foundation.Log;
namespace IEMSOFT.EasyHotel.Admin.Filters
{
    public class ExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        private static ILogger _apiLogService = new SqlLogger("SiteError");
        public   void  OnException (ExceptionContext filterContext)
        {
            var requestId = LogHelper.RequestVariables.GetCurrentRequestId();
            var promptMsg = string.Format("系统内部发生错误！<br/>消息：{0}<br/>请求ID：{1}",
                                            filterContext.Exception.Message,
                                            requestId);

            var controller = "";
            var action = "";
            try
            {
                controller = filterContext.Controller.ControllerContext.RouteData.Values["controller"].ToString();
                action = filterContext.Controller.ControllerContext.RouteData.Values["action"].ToString();
            }
            catch (Exception)
            {
            }

            var errorMsg = string.Format("系统内部发生错误:{0}！请求ID：{1},Controller:{2},Action:{3}",
                                            filterContext.Exception.Message,
                                            requestId,
                                            controller,
                                            action);
            _apiLogService.Error(errorMsg, filterContext.Exception);

            var error = new BasicResult<string, List<string>>();
            error.Msg = new List<string>();
            error.Msg.Add(promptMsg);
            var jsonResult=new JsonNetResult(error);
            filterContext.Result = jsonResult;
          //  filterContext.HttpContext.Response.StatusCode = 200;
            filterContext.ExceptionHandled = true;
        }
    }
}