﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class RoomAddModel
    {
        public int FromRoomNo { get; set; }
        public int RoomCount { get; set; }
        public int RoomTypeId { get; set; }
    }
}