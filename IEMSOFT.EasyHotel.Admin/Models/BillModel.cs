﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class BillModel:BillCreateModel
    {
        public string CheckoutDate { get; set; }
        public int StayTotalDays { get; set; }
        public decimal RoomTotalFee { get; set; }//房间总费用
        public int BillStatus { get; set; }
        public string Created { get; set; }
        public string Modified { get; set; }
    }
}