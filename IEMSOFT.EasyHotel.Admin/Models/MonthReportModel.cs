﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class MonthReportMode
    {
        public string Month { get; set; }
        public int BillsCount { get; set; }
        public decimal BillsRoomTotalFee { get; set; }
        public decimal BillsConsumeTotalFee { get; set; }
        public decimal BillsTotalFee { get; set; }
    }
}