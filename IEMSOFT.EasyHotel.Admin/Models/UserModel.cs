﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public int GroupHotelId { get; set; }
        public int SubHotelId { get; set; }
        public int RoleId { get; set; }
        public string SubHotelName { get; set; }
        public string RoleName { get; set; }
   
    }
}