﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL
{
    public class MySqlDBProvider : IDBProvider, IDisposable
    {
        private PetaPoco.Database _db;
        public MySqlDBProvider(string dbConStrName)
        {
            _db = new PetaPoco.Database(dbConStrName);
            _db.KeepConnectionAlive = false;
        }

        public MySqlDBProvider(string dbConStrName,bool shouldKeepAlive)
        {
            _db = new PetaPoco.Database(dbConStrName);
            _db.KeepConnectionAlive = shouldKeepAlive;
        }

        public void Dispose()
        {
            try
            {
                _db.CloseSharedConnection();
            }
            catch (Exception)
            {

                //ignore
            }
        }

        public PetaPoco.Database DB
        {
            get { return _db; }
        }
    }
}
