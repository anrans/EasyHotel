﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.EasyHotel.DAL.Models;
namespace IEMSOFT.EasyHotel.DAL
{
    public class PayTypeDAL
    {
        private  IDBProvider _dbProvider;
        public PayTypeDAL(IDBProvider dbProvider)
        {
            _dbProvider = dbProvider;
        }

        public List<PayType> GetList()
        {
            var ret = _dbProvider.DB.Fetch<PayType>("select * from PayType ");
            return ret;
        }

        public PayType GetOne(int payTypeId)
        {
            var ret = _dbProvider.DB.FirstOrDefault<PayType>("select * from PayType where PayTypeId=@0 ",payTypeId);
            return ret;
        }
    }
}
